
--注册特殊进攻
ac.unit['特殊进攻怪-50闪避']{
    war3_id = 'h700',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-隐身']{
    war3_id = 'h707',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-拆基地']{
    war3_id = 'h706',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-狙击']{
    war3_id = 'h705',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-魔免']{
    war3_id = 'h702',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-无敌']{
    war3_id = 'h701',
    is_invade_creep = true,
}

ac.unit['特殊进攻怪-物免']{
    war3_id = 'h703',
    is_invade_creep = true,
}

--注册boss
ac.unit['最终Boss-1号']{
    war3_id = 'O106',
    is_invade_creep = true,
    is_invade_boss = true,
    is_final_boss = true,
}

--注册boss
ac.unit['最终Boss-2号']{
    war3_id = 'O107',
    is_invade_creep = true,
    is_invade_boss = true,
    is_final_boss = true,
}
