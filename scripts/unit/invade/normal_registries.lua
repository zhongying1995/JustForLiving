
--注册主城
ac.unit['主城']{
    war3_id = 'h300',
}

--注册boss
ac.unit['Boss进攻怪-第1波']{
    war3_id = 'O101',
    is_invade_creep = true,
    is_invade_boss = true,
}

ac.unit['Boss进攻怪-第2波']{
    war3_id = 'O102',
    is_invade_creep = true,
    is_invade_boss = true,
}

ac.unit['Boss进攻怪-第3波']{
    war3_id = 'O103',
    is_invade_creep = true,
    is_invade_boss = true,
}

ac.unit['Boss进攻怪-第4波']{
    war3_id = 'O104',
    is_invade_creep = true,
    is_invade_boss = true,
}

ac.unit['Boss进攻怪-第5波']{
    war3_id = 'O105',
    is_invade_creep = true,
    is_invade_boss = true,
}

ac.unit['Boss进攻怪-第6波']{
    war3_id = 'O100',
    is_invade_creep = true,
    is_invade_boss = true,
}


--注册普通怪物
ac.unit['普通进攻怪-第1波']{
    war3_id = 'h100',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第2波']{
    war3_id = 'h101',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第3波']{
    war3_id = 'h102',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第4波']{
    war3_id = 'h103',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第5波']{
    war3_id = 'h104',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第6波']{
    war3_id = 'h105',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第7波']{
    war3_id = 'h106',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第8波']{
    war3_id = 'h107',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第9波']{
    war3_id = 'h108',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第10波']{
    war3_id = 'h109',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第11波']{
    war3_id = 'h10A',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第12波']{
    war3_id = 'h10B',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第13波']{
    war3_id = 'h10C',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第14波']{
    war3_id = 'h10D',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第15波']{
    war3_id = 'h10E',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第16波']{
    war3_id = 'h10F',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第17波']{
    war3_id = 'h10G',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第18波']{
    war3_id = 'h10H',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第19波']{
    war3_id = 'h10I',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第20波']{
    war3_id = 'h10J',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第21波']{
    war3_id = 'h10K',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第22波']{
    war3_id = 'h10L',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第23波']{
    war3_id = 'h10M',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第24波']{
    war3_id = 'h10N',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第25波']{
    war3_id = 'h10O',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第26波']{
    war3_id = 'h10P',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第27波']{
    war3_id = 'h10Q',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第28波']{
    war3_id = 'h10R',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第29波']{
    war3_id = 'h10S',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第30波']{
    war3_id = 'h10T',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第31波']{
    war3_id = 'h10U',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第32波']{
    war3_id = 'h10V',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第33波']{
    war3_id = 'h10W',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第34波']{
    war3_id = 'h10X',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第35波']{
    war3_id = 'h10Y',
    is_invade_creep = true,
}

ac.unit['普通进攻怪-第36波']{
    war3_id = 'h10Z',
    is_invade_creep = true,
}