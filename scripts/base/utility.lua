local color_styles = {
    --金黄
    ['reward'] = '|cffff9900',
    --红色
    ['warn'] = '|cffff0000',
    --紫色
    ['importent'] = '|cffff00ff',
    --浅橙
    ['prominent'] = '|cffff9900',
    --灰色
    ['unavailable'] = '|cffc0c0c0',
}

function ac.get_color_string(str, type)
    local str = tostring(str)
    if color_styles[type] then
        str = color_styles[type] .. str .. '|r'
    end
	return str
end