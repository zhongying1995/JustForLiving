local mt = ac.skill['通用暴击']{
    probability = 0,
    rate = 200,
}

function mt:on_add(  )
    local unit = self.owner
    unit:add_buff('通用暴击'){
        probability = self.probability,
        rate = self.rate,
        skill = self,
    }
end

function mt:on_remove(  )
    local unit = self.owner
    unit:remove_buff('通用暴击')
end


local buff = ac.buff['通用暴击']{

}

function buff:on_add(  )
    local unit = self.target
    self.crit = unit:add_crit{
        probability = self.probability,
        rate = self.rate,
        skill = self.skill,
    }
end

function buff:on_remove(  )
    local unit = self.target
    unit:remove_crit(self.crit)
end