local Skill = Router.skill
local lineage_datas = require 'skill.talent.talent_datas'['血脉']

local mt = ac.skill['血脉']{
    ability_id = 'A500',
}

--技能书的技能
local skill_datas = {
    '血脉进阶',
    '轮海',
    '道宫',
    '四极',
    '化龙',
    '仙台',
}

mt.skill_tip = [[
|cffff9900品级|r：%s
|cffff9900全属性|r：+%s
|cffff9900潜能激发|r：%s|cffff9900x|r
    -白色属性得到增加时，会乘以该倍数

|cffff00ff天赋点|r：%s
|cffff00ff通关积分|r：%s
]]

mt.unavailable_tip = [[
|cffff0000英雄没有注入任何血脉|r

|cffff00ff天赋点|r：%s
|cffff00ff通关积分|r：%s
]]

function mt:on_add(  )
    local unit = self.owner
    for _, name in pairs(skill_datas) do
        unit:add_skill(name){}
    end
    self.fresh_tip_timer = ac.loop(1000, function (  )
        if unit:is_alive() then
            self:change_tips()
        end
    end)
end

--改变技能介绍
function mt:change_tips( )
    local unit = self.owner
    local player = unit:get_owner()
    local talent = player:get_talent_point()
    local integral = player:get_clearance_integral()
    local tip
    local lv = player:get_lineage_level()
    local data = lineage_datas[lv]
    if data then
        tip = self.skill_tip:format(data['name'], data['attr'], data['potential'], talent, integral)
    else
        tip = self.unavailable_tip:format(talent, integral)
    end
    if player:is_self() then
        Skill.set_class_tip(self.ability_id, tip)
    end
    unit:fresh_show()
end

function mt:on_remove(  )
    if self.fresh_tip_timer then
        self.fresh_tip_timer:remove()
    end
end