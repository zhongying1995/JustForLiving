local Talent = {}

--血脉数据
Talent['血脉'] = {
    [1] = {
        ['name'] = '凡级',
        ['integral'] = 20,
        ['map_level'] = 2,
        ['attr'] = 30,
        ['potential'] = 1.1,
    },
    [2] = {
        ['name'] = '超凡级',
        ['integral'] = 50,
        ['map_level'] = 3,
        ['attr'] = 80,
        ['potential'] = 1.2,
    },
    [3] = {
        ['name'] = '王级',
        ['integral'] = 120,
        ['map_level'] = 5,
        ['attr'] = 80,
        ['potential'] = 1.4,
    },
    [4] = {
        ['name'] = '人王级',
        ['integral'] = 180,
        ['map_level'] = 6,
        ['attr'] = 210,
        ['potential'] = 1.6,
    },
    [5] = {
        ['name'] = '圣级',
        ['integral'] = 250,
        ['map_level'] = 8,
        ['attr'] = 280,
        ['potential'] = 1.8,
    },
    [6] = {
        ['name'] = '脱圣级',
        ['integral'] = 360,
        ['map_level'] = 9,
        ['attr'] = 360,
        ['potential'] = 2,
    },
    [7] = {
        ['name'] = '神级',
        ['integral'] = 480,
        ['map_level'] = 10,
        ['attr'] = 480,
        ['potential'] = 2.2,
    },
    [8] = {
        ['name'] = '弑神级',
        ['integral'] = 600,
        ['map_level'] = 12,
        ['attr'] = 600,
        ['potential'] = 2.4,
    },
    [9] = {
        ['name'] = '仙',
        ['integral'] = 720,
        ['map_level'] = 13,
        ['attr'] = 720,
        ['potential'] = 2.7,
    },
    [10] = {
        ['name'] = '伐仙级',
        ['integral'] = 850,
        ['map_level'] = 15,
        ['attr'] = 850,
        ['potential'] = 3,
    },
    [11] = {
        ['name'] = '天级',
        ['integral'] = 1000,
        ['map_level'] = 18,
        ['attr'] = 1000,
        ['potential'] = 3.5,
    },
    [12] = {
        ['name'] = '逆天级',
        ['integral'] = 1500,
        ['map_level'] = 20,
        ['attr'] = 1500,
        ['potential'] = 4,
    },
}

--轮海
Talent['轮海'] = {
    [1] = {
        ['name'] = '苦海',
        ['talent'] = 10,
        ['effect'] = '全属性+10',
        ['attr'] = 10,
        ['on_add'] = function ( self, u )
            u:add_all_add_attr(self['attr'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_all_add_attr(-self['attr'])
        end
    },
    [2] = {
        ['name'] = '命泉',
        ['talent'] = 30,
        ['effect'] = '生命值+500',
        ['life'] = 500,
        ['on_add'] = function ( self, u )
            u:add_max_life(self['life'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_max_life(-self['life'])
        end
    },
    [3] = {
        ['name'] = '神桥',
        ['talent'] = 60,
        ['effect'] = '护甲+10',
        ['defence'] = 10,
        ['on_add'] = function ( self, u )
            u:add_add_defence(self['defence'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_defence(-self['defence'])
        end
    },
    [4] = {
        ['name'] = '彼岸',
        ['talent'] = 120,
        ['effect'] = '闪避+5%',
        ['dodge'] = 5,
        ['on_add'] = function ( self, u )
            print('轮海-彼岸级，假装有闪避效果吧，还没做呢')
            -- u:add_dodge(self['dodge'])
        end,
        ['on_remove'] = function ( self, u )
            print('轮海-彼岸级，假装移除闪避效果吧，好像也没差')
            -- u:add_dodge(-self['dodge'])
        end
    },
    [5] = {
        ['name'] = '觉醒轮海',
        ['talent'] = 300,
        ['effect'] = '伤害加成+10%',
        ['rate'] = 10,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将造成伤害-乘除'(function ( trg, damage )
                damage.damage = damage.damage * (100 + self['rate'])/100
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
}


--道宫
Talent['道宫'] = {
    [1] = {
        ['name'] = '心藏',
        ['talent'] = 10,
        ['effect'] = '攻击+50',
        ['atk'] = 50,
        ['on_add'] = function ( self, u )
            u:add_add_attack(self['atk'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_attack(-self['atk'])
        end
    },
    [2] = {
        ['name'] = '肝藏',
        ['talent'] = 30,
        ['effect'] = '生命值+200',
        ['life'] = 200,
        ['on_add'] = function ( self, u )
            u:add_max_life(self['life'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_max_life(-self['life'])
        end
    },
    [3] = {
        ['name'] = '脾藏',
        ['talent'] = 60,
        ['effect'] = '护甲+10',
        ['defence'] = 10,
        ['on_add'] = function ( self, u )
            u:add_add_defence(self['defence'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_defence(-self['defence'])
        end
    },
    [4] = {
        ['name'] = '肺藏',
        ['talent'] = 120,
        ['effect'] = '闪避+5%',
        ['dodge'] = 5,
        ['on_add'] = function ( self, u )
            print('道宫-肺藏级，假装有闪避效果+5%吧，还没做呢')
            -- u:add_dodge(self['dodge'])
        end,
        ['on_remove'] = function ( self, u )
            print('道宫-肺藏级，假装移除闪避效果-5%吧，好像也没差')
            -- u:add_dodge(-self['dodge'])
        end
    },
    [5] = {
        ['name'] = '肾藏',
        ['talent'] = 150,
        ['effect'] = '暴击概率+8%',
        ['rate'] = 8,
        ['on_add'] = function ( self, u )
            print('道宫-肾藏级，假装有额外暴击+8%吧，还没做呢')
        end,
        ['on_remove'] = function ( self, u )
            print('道宫-肾藏级，假装移除额外暴击-8%吧，好像也没差')
        end
    },
    [6] = {
        ['name'] = '觉醒道宫',
        ['talent'] = 400,
        ['effect'] = '伤害加成+15%',
        ['rate'] = 15,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将造成伤害-乘除'(function ( trg, damage )
                damage.damage = damage.damage * (100 + self['rate'])/100
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
}

--四极
Talent['四极'] = {
    [1] = {
        ['name'] = '神拳-左',
        ['talent'] = 10,
        ['effect'] = '攻击+50',
        ['atk'] = 50,
        ['on_add'] = function ( self, u )
            u:add_add_attack(self['atk'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_attack(-self['atk'])
        end
    },
    [2] = {
        ['name'] = '神足-左',
        ['talent'] = 30,
        ['effect'] = '生命值+500',
        ['life'] = 500,
        ['on_add'] = function ( self, u )
            u:add_max_life(self['life'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_max_life(-self['life'])
        end
    },
    [3] = {
        ['name'] = '神拳-右',
        ['talent'] = 60,
        ['effect'] = '护甲+10',
        ['defence'] = 10,
        ['on_add'] = function ( self, u )
            u:add_add_defence(self['defence'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_defence(-self['defence'])
        end
    },
    [4] = {
        ['name'] = '神拳-右',
        ['talent'] = 120,
        ['effect'] = '闪避+5%',
        ['dodge'] = 5,
        ['on_add'] = function ( self, u )
            print('四极-神拳-右级，假装有闪避效果+5%吧，还没做呢')
            -- u:add_dodge(self['dodge'])
        end,
        ['on_remove'] = function ( self, u )
            print('四极-神拳-右级，假装移除闪避效果-5%吧，好像也没差')
            -- u:add_dodge(-self['dodge'])
        end
    },
    [5] = {
        ['name'] = '觉醒四极',
        ['talent'] = 300,
        ['effect'] = '伤害加成+10%',
        ['rate'] = 10,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将造成伤害-乘除'(function ( trg, damage )
                damage.damage = damage.damage * (100 + self['rate'])/100
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
}


--化龙
Talent['化龙'] = {
    [1] = {
        ['name'] = '龙一变',
        ['talent'] = 10,
        ['effect'] = '全属性+10',
        ['attr'] = 10,
        ['on_add'] = function ( self, u )
            u:add_all_add_attr(self['attr'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_all_add_attr(-self['attr'])
        end
    },
    [2] = {
        ['name'] = '龙二变',
        ['talent'] = 30,
        ['effect'] = '生命值+500',
        ['life'] = 500,
        ['on_add'] = function ( self, u )
            u:add_max_life(self['life'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_max_life(-self['life'])
        end
    },
    [3] = {
        ['name'] = '龙三变',
        ['talent'] = 60,
        ['effect'] = '护甲+10',
        ['defence'] = 10,
        ['on_add'] = function ( self, u )
            u:add_add_defence(self['defence'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_add_defence(-self['defence'])
        end
    },
    [4] = {
        ['name'] = '龙四变',
        ['talent'] = 120,
        ['effect'] = '闪避+5%',
        ['dodge'] = 5,
        ['on_add'] = function ( self, u )
            print('龙四变，假装有闪避效果+5%吧，还没做呢')
            -- u:add_dodge(self['dodge'])
        end,
        ['on_remove'] = function ( self, u )
            print('龙四变，假装移除闪避效果-5%吧，好像也没差')
            -- u:add_dodge(-self['dodge'])
        end
    },
    [5] = {
        ['name'] = '龙五变',
        ['talent'] = 150,
        ['effect'] = '暴击率+8%',
        ['rate'] = 8,
        ['on_add'] = function ( self, u )
            print('龙五变，假装增加暴击几率 +8%')
        end,
        ['on_remove'] = function ( self, u )
            print('龙五变，假装增加暴击几率 +8%')
        end
    },
    [6] = {
        ['name'] = '龙六变',
        ['talent'] = 180,
        ['effect'] = '暴击伤害+50%',
        ['rate'] = 50,
        ['on_add'] = function ( self, u )
            print('龙六变，假装增加暴击伤害 +50%')
        end,
        ['on_remove'] = function ( self, u )
            print('龙六变，假装去除了暴击伤害 +50%')
        end
    },
    [7] = {
        ['name'] = '龙七变',
        ['talent'] = 210,
        ['effect'] = '移动速度+80',
        ['move_speed'] = 80,
        ['on_add'] = function ( self, u )
            u:add_move_speed(self['move_speed'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_move_speed(-self['move_speed'])
        end
    },
    [8] = {
        ['name'] = '龙八变',
        ['talent'] = 240,
        ['effect'] = '伤害抵挡+50',
        ['damage_absorb'] = 50,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将受到伤害-加减'(function ( trg, damage )
                if damage.damage > self['damage_absorb'] then
                    damage.damage = damage.damage - self['damage_absorb']
                else
                    damage.damage = 0
                end
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
    [9] = {
        ['name'] = '龙九变',
        ['talent'] = 270,
        ['effect'] = '攻击速度+120%',
        ['attack_speed'] = 120,
        ['on_add'] = function ( self, u )
            u:add_attack_speed(self['attack_speed'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_attack_speed(-self['attack_speed'])
        end
    },
    [10] = {
        ['name'] = '化龙觉醒',
        ['talent'] = 500,
        ['effect'] = '伤害吸血+2%',
        ['rate'] = 2,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将造成伤害效果'(function ( trg, damage )
                local heal = damage.damage * self['rate'] / 100
                u:heal{
                    heal = heal,
                    skill = true,
                }
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
}


--仙台
Talent['仙台'] = {
    [1] = {
        ['name'] = '仙一阶',
        ['talent'] = 10,
        ['effect'] = '攻击速度+10%',
        ['attack_speed'] = 10,
        ['on_add'] = function ( self, u )
            u:add_attack_speed(self['attack_speed'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_attack_speed(-self['attack_speed'])
        end
    },
    [2] = {
        ['name'] = '仙二阶',
        ['talent'] = 30,
        ['effect'] = '魔法值+500',
        ['mana'] = 500,
        ['on_add'] = function ( self, u )
            u:add_max_mana(self['mana'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_max_mana(-self['mana'])
        end
    },
    [3] = {
        ['name'] = '仙三阶',
        ['talent'] = 60,
        ['effect'] = '魔法回复速度+5',
        ['rec'] = 5,
        ['on_add'] = function ( self, u )
            u:add_mana_recovery(self['rec'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_mana_recovery(-self['rec'])
        end
    },
    [4] = {
        ['name'] = '仙四阶',
        ['talent'] = 120,
        ['effect'] = '暴击率+6%',
        ['rate'] = 6,
        ['on_add'] = function ( self, u )
            print('仙四阶，假装增加暴击几率+6%吧，还没做呢')
            -- u:add_dodge(self['dodge'])
        end,
        ['on_remove'] = function ( self, u )
            print('仙四阶，假装去除暴击几率 -6%吧，好像也没差')
            -- u:add_dodge(-self['dodge'])
        end
    },
    [5] = {
        ['name'] = '仙五阶',
        ['talent'] = 150,
        ['effect'] = '暴击伤害+40%',
        ['crit'] = 8,
        ['on_add'] = function ( self, u )
            print('仙五阶，假装增加暴击伤害 +40%')
        end,
        ['on_remove'] = function ( self, u )
            print('仙五阶，假装减少暴击伤害 -40%')
        end
    },
    [6] = {
        ['name'] = '仙六阶',
        ['talent'] = 180,
        ['effect'] = '移动速度+60',
        ['move_speed'] = 60,
        ['on_add'] = function ( self, u )
            u:add_move_speed(self['move_speed'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_move_speed(self['-move_speed'])
        end
    },
    [7] = {
        ['name'] = '仙七阶',
        ['talent'] = 210,
        ['effect'] = '伤害抵挡+40',
        ['damage_absorb'] = 210,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将受到伤害-加减'(function ( trg, damage )
                if damage.damage > self['damage_absorb'] then
                    damage.damage = damage.damage - self['damage_absorb']
                else
                    damage.damage = 0
                end
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
    [8] = {
        ['name'] = '仙八阶',
        ['talent'] = 240,
        ['effect'] = '生命恢复+30',
        ['rec'] = 30,
        ['on_add'] = function ( self, u )
            u:add_life_recovery(self['rec'])
        end,
        ['on_remove'] = function ( self, u )
            u:add_life_recovery(-self['rec'])
        end
    },
    [9] = {
        ['name'] = '仙九阶',
        ['talent'] = 270,
        ['effect'] = '减防+50',
        ['def'] = 50,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-攻击'(function ( trg, atter, atted )
                
            end)
            print('仙九阶,假装攻击可以减少防御')
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            print('仙九阶,假装攻击可以减少防御的效果被取消了')
        end
    },
    [10] = {
        ['name'] = '化龙觉醒',
        ['talent'] = 600,
        ['effect'] = '伤害吸血+3%',
        ['rate'] = 3,
        ['on_add'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
            u._talent_trg[self.name] = u:event '单位-即将造成伤害效果'(function ( trg, damage )
                local heal = damage.damage * self['rate'] / 100
                u:heal{
                    heal = heal,
                    skill = true,
                }
            end)
        end,
        ['on_remove'] = function ( self, u )
            if not u._talent_trg then
                u._talent_trg = {}
            end
            if u._talent_trg[self.name] then
                u._talent_trg[self.name]:remove()
            end
        end
    },
}


return Talent