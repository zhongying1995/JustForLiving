local Skill = Router.skill
local table_insert = table.insert
local all_talent_datas = require 'skill.talent.talent_datas'

--注册函数
--  @技能表，buff表
return function ( name, ability_id )
        
    local SKILL_NAME = name
    
    local talent_datas = all_talent_datas[SKILL_NAME]

    local skill = ac.skill[SKILL_NAME]{
        ability_id = ability_id,
        buff_name = SKILL_NAME,
        unavailable_tip = [=[|cffc0c0c0[封印的|%.f天赋点解封]|r]=],
        available_tip = [=[|cffff9900[已解封]|r]=],
    }

    function skill:on_add(  )
        local unit = self.owner
        local player = unit:get_owner()
        local level = player:get_talent_skill_level(self.name)
        if level > 0 then
            unit:add_buff(self.buff_name){
                level = level,
                skill = self,
            }
        end
        self:fresh_tip()
    end

    function skill:on_effect(  )
        local unit = self.owner
        local player = unit:get_owner()
        local level = player:get_talent_skill_level(self.name)
        local next_level = level + 1
        local max_level = #talent_datas
        data = talent_datas[next_level]
        if data then
            local talent = player:get_talent_point()
            local need_talent = data['talent']
            if talent >= need_talent then
                player:add_talent_point(-need_talent)
                unit:add_buff(self.buff_name){
                    level = next_level,
                    skill = self,
                }
                player:add_talent_skill_level(self.name)
                self:fresh_tip()
            else
                local tip = ('天赋点不够，缺|cffff0000%s|r点'):format(need_talent-talent)
                player:send_msg(tip, 3)
            end
        end
    end

    function skill:fresh_tip(  )
        local unit = self.owner
        local player = unit:get_owner()
        local lv = player:get_talent_skill_level(self.name)
        local max_level = #talent_datas
        local tips = {}

        for i = 1, max_level do
            local data = talent_datas[i]
            local tip
            if lv >= i then
                tip = self.available_tip
            else
                tip = self.unavailable_tip:format(data['talent'])
            end
            tip = tip .. data['name'] .. '：' .. data['effect']
            table_insert( tips, tip )
        end
        local tip = table.concat( tips, '\n' )

        if player:is_self() then
            Skill.set_class_tip(self.ability_id, tip)
        end
    end

    function skill:on_remove(  )
        local unit = self.owner
        unit:remove_buff(self.buff_name)
    end

    local buff = ac.buff[SKILL_NAME]{

    }

    function buff:on_add(  )
        local unit = self.target
        local level = self.level
        for i = 1, level do
            local data = talent_datas[i]
            data['on_add'](data, unit)
        end
    end

    function buff:on_remove(  )
        local unit = self.target
        local level = self.level
        for i = 1, level do
            local data = talent_datas[i]
            data['on_remove'](data, unit)
        end
    end

    return 
end
