local Skill = Router.skill
local lineage_datas = require 'skill.talent.talent_datas'['血脉']

local mt = ac.skill['血脉进阶']{
    ability_id = 'A501',
}

mt.skill_tip = [[
解封：|cffff00ff%s|r
|cffff9900需求|r
    -通关积分：%s   
    -地图等级：%s
|cffff9900效果|r
    -全属性：%.f
    -潜能激发：%.1fx
]]

mt.max_level_tips = [[
|cffff9900已达血脉上限|r
|cffff00ff%s|r
    -全属性：%.f
    -潜能激发：%.1fx
]]


function mt:on_add(  )
    local unit = self.owner
    local player = unit:get_owner()
    local lv = player:get_lineage_level()
    local next_lv = lv + 1
    
    self:change_upgrade_tip( next_lv)
    if lv > 0 then
        self:upgrade_reward(lv)
    end
end

--根据等级，给予英雄奖励
function mt:upgrade_reward( lv )
    local unit = self.owner
    local data = lineage_datas[lv]
    if data then
        local attr = data['attr']
        unit:add_all_add_attr(attr)

        local rate = data['potential']

        if not unit._lineage_add_str then
            unit._lineage_add_str = unit.add_str
        end
        function unit:add_str(str)
            str = (str or 1) * rate
            self:_lineage_add_str(str)
        end

        if not unit._lineage_add_agi then
            unit._lineage_add_agi = unit.add_agi
        end
        function unit:add_agi(agi)
            agi = (agi or 1) * rate
            self:_lineage_add_agi(agi)
        end

        if not unit._lineage_add_int then
            unit._lineage_add_int = unit.add_int
        end
        function unit:add_int(int)
            int = (int or 1) * rate
            self:_lineage_add_int(int)
        end


    end
end

function mt:on_effect(  )
    local unit = self.owner
    local player = unit:get_owner()
    local lv = player:get_lineage_level()
    local next_lv = lv + 1
    local data = lineage_datas[next_lv]

    if data then
        local map_level = player:get_map_level()
        local integral = player:get_clearance_integral()
        local need_level = data['map_level']
        local need_integral = data['integral']

        --成功进阶
        if map_level >= need_level and integral >= need_integral then
            player:add_clearance_integral(-need_integral)
            player:add_lineage_level()
            self:change_upgrade_tip( next_lv+1)

            self:upgrade_reward(next_lv)

        else
            local msg
            if map_level < need_level then
                msg = ('地图等级至少需要%.f级'):format(need_level)
            end
            if integral < need_integral then
                local msg_1 = ('通关积分至少需要%.f点'):format(need_integral)
                if msg then
                    msg = msg .. ' | ' .. msg_1
                else
                    msg = msg_1
                end
            end
            self:fail_upgrade(msg)
        end
        
    end
end

--更改技能的升级介绍
function mt:change_upgrade_tip( lv )
    local unit = self.owner
    local player = unit:get_owner()
    local data = lineage_datas[lv]
    local tip
    if data then
        local map_level = player:get_map_level()
        local integral = player:get_clearance_integral()
        local need_level = data['map_level']
        local need_integral = data['integral']
        local color_type
        if map_level < need_level then
            color_type = 'unavailable'
        else
            color_type = 'prominent'
        end
        need_level = ac.get_color_string(tostring(need_level), color_type)

        if integral < need_integral then
            color_type = 'unavailable'
        else
            color_type = 'prominent'
        end
        need_integral = ac.get_color_string(tostring(need_integral), color_type)

        tip = self.skill_tip:format(data['name'], need_integral, need_level, data['attr'], data['potential'])
    else
        data = lineage_datas[#lineage_datas]
        tip = self.max_level_tips:format(data['name'], data['attr'], data['potential'])
    end
    if player:is_self() then
        Skill.set_class_tip(self.ability_id, tip)
    end
end

function mt:fail_upgrade( msg )
    local unit = self.owner
    local player = unit:get_owner()
    msg = ac.get_color_string('血脉进阶失败', 'warn') .. ':' .. msg
    player:send_msg(msg, 3)
end


function mt:on_remove(  )
    
end