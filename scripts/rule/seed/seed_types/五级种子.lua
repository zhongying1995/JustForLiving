ac.unit['五级种子']{
    war3_id = 'h60A',
    fruit = 'I131',
    skill_names = '生长',
    grow_duration = 300,
}

local mt = ac.item['五级种子']{
    is_seed = true,
    positive = true,
    stack = 1,
    perishable = true,
    art = [[ReplaceableTextures\CommandButtons\BTNAcorn.blp]],
    tip = [[五级种子可以种出五级果实]],
    title = '五级种子',
    target_type = ac.skill.TARGET_TYPE_POINT,
    range = 100,
}

function mt:on_effect(  )
    local point = self.target
    local unit = self.owner
    ac.player[16]:create_unit(self.name, point)
end