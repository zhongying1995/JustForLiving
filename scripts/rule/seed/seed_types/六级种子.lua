ac.unit['六级种子']{
    war3_id = 'h609',
    fruit = 'I132',
    skill_names = '生长',
    grow_duration = 360,
}

local mt = ac.item['六级种子']{
    is_seed = true,
    positive = true,
    stack = 1,
    perishable = true,
    art = [[ReplaceableTextures\CommandButtons\BTNAcorn.blp]],
    tip = [[六级种子可以种出六级果实]],
    title = '六级种子',
    target_type = ac.skill.TARGET_TYPE_POINT,
    range = 100,
}

function mt:on_effect(  )
    local point = self.target
    local unit = self.owner
    ac.player[16]:create_unit(self.name, point)
end