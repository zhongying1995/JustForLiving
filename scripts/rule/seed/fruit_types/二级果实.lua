local mt = ac.item['二级果实']{
    all_attr = 10,
    is_fruit = true,
    positive = true,
    stack = 1,
    perishable = true,
    art = [[ReplaceableTextures\CommandButtons\BTNOrbOfVenom.blp]],
    tip = [[二级果实，英雄吃掉后，全属性+10]],
    title = '二级果实',
}

function mt:on_effect(  )
    local unit = self.owner
    local hero = unit:get_owner().hero
    hero:add_str(self.all_attr)
    hero:add_agi(self.all_attr)
    hero:add_int(self.all_attr)
end