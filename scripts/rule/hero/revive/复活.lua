local math_tointeger = math.tointeger
local Player = Router.player


local Revive = require 'rule.hero.revive.revive'

local mt = ac.skill['复活石-复活']{
    ability_id = 'A001',
    tip = [[
当前可用复活次数：%remainder_times%
复活币：
    - %revive_coin%
]],
    remainder_times = function ( self )
        return self.owner:get_owner():get_remainder_revive_times()
    end,
    revive_coin = function ( self )
        return self.owner:get_owner():get_revive_coin()
    end,

    disable_art = [[ReplaceableTextures\CommandButtons\BTNCancel.blp]],
    enable_art = [[ReplaceableTextures\CommandButtons\BTNAnkh.blp]],
    
}


function mt:fresh_using_art(  )
    if self.owner:get_owner():get_remainder_revive_times() == 0 and self.owner:get_owner():get_revive_coin() == 0 then
        self.art = self.disable_art
    else
        self.art = self.enable_art
    end
    if self:get('last_art') ~= self.art then
        self:fresh_art(self.art)
        self:set('last_art', self.art)
    end
end


function mt:on_effect(  )
    local unit = self.owner
    local player = unit:get_owner()
    if player:get_remainder_revive_times() == 0 then
        if player:get_revive_coin() == 0 then
            return
        else
            player:add_revive_coin(-1)
        end
    else
        player:add_remainder_revive_times(-1)
    end

    local hero = player.hero
    if hero:is_alive() then
        log.error('英雄处于复活状态，但是复活石却可以被操控')
        return
    end

    local point = unit:get_point()
    Revive:hero_revive(hero, point)
    self:fresh_using_art()
end


--英雄默认有一次复活次数
Player.__index._remainder_revive_times = 1

--获取英雄复活次数
function Player.__index:get_remainder_revive_times(  )
    return self._remainder_revive_times
end

--设置英雄复活次数
function Player.__index:set_remainder_revive_times( times )
    self._remainder_revive_times = times
end

--增加英雄复活次数
function Player.__index:add_remainder_revive_times( times )
    self:set_remainder_revive_times(self:get_remainder_revive_times() + times)
end


Player.__index._revive_coin = 0
--获取玩家复活币
function Player.__index:get_revive_coin(  )
    return self._revive_coin
end

function Player.__index:set_revive_coin( n )
    self._revive_coin = n
end

function Player.__index:add_revive_coin( n )
    self:set_revive_coin(self:get_revive_coin() + n)
end
