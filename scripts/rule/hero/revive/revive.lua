local table_insert = table.insert
local table_remove = table.remove
local Unit = Router.unit
local Player = Router.player

local Revive = {}
setmetatable(Revive, Revive)

local mt = {}
Revive.__index = mt

--复活机制：
--[[
英雄死亡，在地上出现复活十字架，英雄点击在复活点复活
使用次数有上限，可以使用复活币代替
--]]

--单位复活的默认点
mt._revive_point = nil

--当前死亡的英雄
mt.all_dead_hero = {}

--游戏即将失败倒计时
mt.prepare_game_end_count = 15

--创建复活英雄模块
function mt:create()
    self.hero_revive_listener_trg = ac.game:event '单位-死亡'(function(trg, unit, killer)
        print('单位被击杀啦:', unit:get_name(), unit:is_hero())
        if unit:is_hero() and not unit:is_illusion() and unit:get_team() == 1 then
            print('单位死亡,', unit:is_hero(), unit, unit:get_name())
            self:hero_dead(unit)
            self:check_game_end()
        end
    end)
    self._revive_point = ac.point(100, 100)
    self.player_counts = Player.count_alive()
    print('当前游戏人数：', self.player_counts)
end

--英雄死亡，创建其复活十字架
function mt:hero_dead( unit )
    local point = unit:get_point()
    local player = unit:get_owner()
    local stone = player:show_revive_stone()
    stone:set_point(point)
    self.all_dead_hero[unit] = true
end

--复活英雄
function mt:hero_revive( unit, point )
    local player = unit:get_owner()
    player:show_revive_stone(false)
    self.all_dead_hero[unit] = nil
    unit:revive(point, true)
    local msg = ('英雄[%s]复活了!'):format(unit:get_name())
    ac.player.self:send_msg(msg, 3)
    self:stop_prepare_game_end()
end

--检查游戏失败条件
function mt:check_game_end(  )
    local n = table.getn(self.all_dead_hero)
    if n == self.player_counts then
        self:prepare_game_end()
    end
end

--停止游戏即将结束
function mt:stop_prepare_game_end(  )
    if self.prepare_game_end_timer then
        self.prepare_game_end_timer:remove()
        self.prepare_game_end_timer = nil
    end
end

--即将游戏结束
function mt:prepare_game_end(  )
    local prepare_game_end_count = self.prepare_game_end_count
    self.prepare_game_end_timer = ac.timer(1000, prepare_game_end_count, function (  )
        local msg = ac.get_color_string(('当前没有存活英雄，游戏即将失败！倒计时：%.f'):format(prepare_game_end_count), 'warn')
        ac.player.self:send_msg(msg, 5)
        if prepare_game_end_count == 0 then
            print('游戏失败了！！！')
            ac.game:event_notify('游戏-失败')
        end
        prepare_game_end_count = prepare_game_end_count - 1
    end)
    self.prepare_game_end_timer:on_timer()
end


--复活石
Player.__index._revive_stone = nil

--获取复活石
function Player.__index:get_revive_stone()
    if not self._revive_stone then
        self._revive_stone = self:create_unit('复活石', ac.point(0, 0))
        self._revive_stone:show(false)
    end
    return self._revive_stone
end

--显示隐藏复活石
function Player.__index:show_revive_stone( is_show )
    local stone = self:get_revive_stone()
    stone:show(is_show)
    return stone
end


local function init()
    ac.game:event '游戏-开始'(function(trg)
        Revive:create()
    end)
end

init()


return Revive