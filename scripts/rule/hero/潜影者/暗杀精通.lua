local Texttag = Router.texttag

local mt = ac.skill['暗杀精通']{
    ability_id = 'A30D',

    tip = [[
潜影者精通刺杀之道，每次攻击很可能秒杀敌人。
被动：有%rate%%几率秒杀普通单位，对boss造成其最大生命值%boss_life_rate%%伤害。
主动：概率翻倍，持续%duration%秒。
冷却%cool%秒。
]],
    rate = 10,
    minion_life_rate = 100,
    boss_life_rate = 10,
    duration = 5,
    cool = 60,
}

function mt:on_add()
    local unit = self.owner
    unit:add_buff('暗杀精通'){
        skill = self,
        rate = self.rate,
        minion_life_rate = self.minion_life_rate,
        boss_life_rate = self.boss_life_rate,
    }
    
end

function mt:on_effect()
    local unit = self.owner
    unit:add_buff('暗杀精通'){
        skill = self,
        time = self.duration,
        rate = self.rate * 2,
        minion_life_rate = self.minion_life_rate,
        boss_life_rate = self.boss_life_rate,
    }
end

function mt:on_remove()
    local unit = self.owner
    unit:remove_buff('暗杀精通')
end


local buff = ac.buff['暗杀精通']{
    model = [[Abilities\Weapons\AvengerMissile\AvengerMissile.mdl]],
    cover_type = 1,
    cover_max = 1,
}

function buff:on_add(  )
    local unit = self.target
    self.ef = unit:add_effect(self.model, 'weapon')
    self.damage_trg = unit:event '单位-即将造成伤害-加减'(function ( trg, damage )
        if damage:is_attack() then
            if self.rate >= math.random(1, 100) then
                local target = damage.target
                local life_rate = self.minion_life_rate
                if target:is_type_hero() then
                    life_rate = self.boss_life_rate
                end
                damage.damage = damage.damage + target:get_max_life() * life_rate / 100
                local tt = Texttag:new{
                    text = '暗杀',
                    player = unit:get_owner(),
                    angle = 90,
                    speed = 5,
                    size = 12,
                    red = 255,
                    green = 0,
                    blue = 0,
                    show = Texttag.SHOW_ALL,
                    life = 1,
                    target = target,
                }
                tt:jump(2, 0.2)
            end
        end
    end)
end

function buff:on_cover( new )
    if new.rate > self.rate then
        return true
    end
end

function buff:on_remove(  )
    if self.ef then
        self.ef:remove()
    end
    if self.damage_trg then
        self.damage_trg:remove()
    end
end

