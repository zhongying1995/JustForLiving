local mt = ac.skill['致命一击']{
    ability_id = 'A30B',
    positive = false,
    passive = true,
    tip = [[
当黑暗舞者破隐攻击时，会额外造成%damage%点伤害。
]],
    agi_rate = {1, 2, 3, 4, 5},
    max_level = 5,
    damage = function (self)
        return self.owner:get_agi(true) * self.agi_rate
    end,
}

function mt:get_damage()
    return self.owner:get_agi(true) * self.agi_rate
end

function mt:on_enable()
    local unit = self.owner
    unit:add_buff('潜影者-致命一击'){
        skill = self,
    }
end

function mt:on_disable()
    local unit = self.owner
    unit:remove_buff('潜影者-致命一击')
end

local mt = ac.buff['潜影者-致命一击']{
    model = [[Abilities\Spells\Items\OrbDarkness\OrbDarkness.mdl]],
}

function mt:on_add()
    local unit = self.target
    self.ef = unit:add_effect(self.model, 'weapon')
end

function mt:on_remove()
    local unit = self.target
    if self.ef then
        self.ef:remove()
    end
end