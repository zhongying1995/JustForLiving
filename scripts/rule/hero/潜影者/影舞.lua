local mt = ac.skill['影舞']{
    ability_id = 'A30C',
    tip = [[
黑暗舞者隐身于虚空中，破隐时，此次普攻必定暴击，如果目标死亡，则再次进入隐身。隐身最长%duration%秒，技能冷却%cool%秒。
]],
    duration = 8,
    cool = 30,
}

function mt:on_effect()
    local unit = self.owner
    unit:add_buff '影舞-隐身'{
        time = self.duration,
        skill = self,
    }
end


local mt = ac.buff['影舞-隐身']{
    effect_model = [[Abilities\Spells\Undead\DeathCoil\DeathCoilSpecialArt.mdl]],
}

function mt:on_add()
    local unit = self.target
    unit:add_effect(self.effect_model, 'origin'):remove()
    unit:add_restriction '隐身'
    --强制暴击
    unit:force_crit()

    self.trg_1 = unit:event '单位-即将造成伤害-加减'(function ( trg, damage )
        if damage:is_attack() then
            local buf = unit:find_buff('潜影者-致命一击')
            if buf then
                damage.damage = damage.damage + buf.skill:get_damage()
            end
        end
    end)
    self.trg_2 = unit:event '单位-即将造成伤害效果'(function ( trg, damage )
        if damage:is_attack() then
            self:remove()
            if damage.damage + 0.5 > damage.target:get_life() then
                unit:add_buff '影舞-隐身'{
                    time = self.skill.duration,
                    skill = self.skill,
                }
            end
        end
    end)
end

function mt:on_remove()
    local unit = self.target
    unit:remove_restriction '隐身'
    --取消强制暴击
    unit:unforce_crit()
    
    if self.trg_1 then
        self.trg_1:remove()
    end
    if self.trg_2 then
        self.trg_2:remove()
    end
end