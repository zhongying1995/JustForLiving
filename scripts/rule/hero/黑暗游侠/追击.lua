local mt = ac.skill['追击']{
    ability_id = 'A305',
    rate = {5, 10, 15, 20, 25},
    arrow_acount = {1, 2, 2, 2, 3},
    passive = true,
    tip = [[
对单位进行普通时有%rate%%额外射出%arrow_acount%箭。
]]
}

function mt:on_enable()
    local unit = self.owner
    unit:add_buff('黑暗游侠-追击'){
        skill = self,
    }
end

function mt:on_disable()
    local unit = self.owner
    unit:remove_buff('黑暗游侠-追击')
end


local mt = ac.buff['黑暗游侠-追击']{
    effect_model = [[Abilities\Spells\Other\BlackArrow\BlackArrowMissile.mdl]],
    speed = 900,
}

function mt:on_add()
    local unit = self.target
    local skill = self.skill
    self.trg = unit:event '单位-即将造成伤害效果'(function(trg, damage)
        if not damage:is_attack() then
            return
        end
        local rate = skill.rate
        if rate <= math.random(1, 100) then
            return
        end
        local target = damage.target
        local high = unit:get_slk('launchZ', 60) + unit:get_high()
        local arrow_acount = skill.arrow_acount
        local t = ac.timer(0.25*1000, arrow_acount, function (  )
            local mvr = ac.mover.target{
                start = unit,
                model = self.effect_model,
                target = target,
                high = high,
                speed = self.speed,
                skill = skill,
            }
            if mvr then
                function mvr:on_finish()
                    unit:damage{
                        target = self.target,
                        damage = unit:get_agi(true),
                        damage_type = '魔法',
                    }
                end
            end
        end)
        t:on_timer()
    end)
end

function mt:on_remove()
    if self.trg then
        self.trg:remove()
    end
end