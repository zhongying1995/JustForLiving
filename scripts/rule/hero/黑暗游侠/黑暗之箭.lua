local mt = ac.skill['黑暗之箭']{
    ability_id = 'A306',
    effect_model = [[Abilities\Spells\Other\BlackArrow\BlackArrowMissile.mdl]],
    hit_radius = 128,
    tip = [[
黑暗射手射出黑暗气息弥漫的一箭,对一跳直线的敌人造成%damage%的伤害,若黑暗之箭击杀了怪物则会诞生一个黑暗之奴,黑暗之奴拥有%minion_life%点生命值与%minion_attack%点攻击力和%minion_defence%点护甲,持续%minion_life_time%秒。冷却%cool%秒。
]],
    damage = function ( self )
        return 1000 + self.owner:get_agi(true) * 3
    end,
    minion_life = function ( self )
        return self.owner:get_agi(true) * 5
    end,
    minion_attack = function ( self )
        return self.owner:get_agi(true)
    end,
    minion_defence = function ( self )
        return self.owner:get_agi(true) / 50
    end,
    minion_life_time = 10,
    minion_name = '黑暗之奴-黑暗之箭',
    cool = 15,
}

function mt:on_effect()
    local unit = self.owner
    local target = self.target
    local ang = unit:get_point() / target
    local high = unit:get_slk('launchZ') + unit:get_high()
    local mvr = ac.mover.line{
        source = unit,
        model = self.effect_model,
        angle = ang,
        distance = 2000,
        high = high,
        speed = 900,
        skill = self,
        hit_area = self.hit_radius,
        hit_type = ac.mover.HIT_TYPE_ENEMY,
    }
    if mvr then
        local damage = self.damage
        local skill = self
        function mvr:on_hit(target)
            target:event '单位-即将受到致命伤害'(function ( trg, damage )
                if damage.skill == skill then
                    local minion = unit:get_owner():create_unit(skill.minion_name, target:get_point())
                    minion:set_max_life(skill.minion_life)
                    minion:set_attack(skill.minion_attack)
                    minion:set_defence(skill.minion_defence)
                    minion:set_time_life(skill.minion_life_time)
                end
                trg:remove()
            end)
            unit:damage{
                target = target,
                damage = damage,
                skill = skill,
                damage_type = '魔法',
            }
        end
    end

end