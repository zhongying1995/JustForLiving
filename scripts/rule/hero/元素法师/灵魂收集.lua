local mt = ac.skill['灵魂收集']{
    ability_id = 'A300',
    passive = true,
    positive = false,
    tip = [[
每击杀一个普通单位永久增加%minion_int_add%点的智力，如果击杀英雄单位则获得%boss_int_add%点的智力。
当前增加：%extra_int%
]],
    max_level = 5,
    minion_int_add = {0.2, 0.4, 0.6, 0.8, 1},
    boss_int_add = {2, 4, 6, 8, 10},
    extra_int = function ( self )
        return self.owner._linghunshouji_extra_int or 0
    end
}

function mt:on_enable()
    local unit = self.owner
    unit:add_buff('元素法师-灵魂收集'){
        skill = self,
    }
end

function mt:on_disable()
    local unit = self.owner
    unit:remove_buff('元素法师-灵魂收集')
end


local mt = ac.buff['元素法师-灵魂收集']{
    effect_model = [[Abilities\Spells\NightElf\SpiritOfVengeance\SpiritOfVengeanceBirthMissile.mdl]],
}

function mt:on_add()
    local unit = self.target
    local skill = self.skill
    self.add_int_trg = unit:event '单位-杀死单位'(function(trg, killer, killed)
        local mvr = ac.mover.target{
			source = killed,
			target = killer,
			model = self.effect_model,
            speed = 500,
            high = 50,
			angle = killed:get_point() / killer:get_point(),
			max_distance = 3000,
			skill = skill,
        }
        
        if mvr then
            function mvr:on_finish()
                local int_add = skill.minion_int_add
                if killed:is_type_hero() then
                    int_add = skill.boss_int_add
                end
                if not unit._linghunshouji_extra_int_sum then
                    unit._linghunshouji_extra_int_sum = 0
                    unit._linghunshouji_extra_int = 0
                end
                unit._linghunshouji_extra_int_sum = unit._linghunshouji_extra_int_sum + int_add
                local int = math.floor(unit._linghunshouji_extra_int_sum)
                if int ~= 0 then
                    unit:add_add_int(int)
                    unit._linghunshouji_extra_int_sum = unit._linghunshouji_extra_int_sum - int
                    unit._linghunshouji_extra_int = unit._linghunshouji_extra_int + int
                end
            end
        end
    end)
end

function mt:on_remove()
    if self.add_int_trg then
        self.add_int_trg:remove()
    end
end