local mt = ac.skill['圣龙降临']{
    ability_id = 'A303',
    
    tranform_id = 'O202',
    tranform_effect_model = [[Abilities\Spells\Orc\FeralSpirit\feralspirittarget.mdl]],
    tip = [[
自身化身成可怖的巨龙，获得%damage_reduce_rate%%的伤害降低，并且每秒对附近%radius%码内敌人造成%damage%点伤害同时减少其%de_attack_speed%%攻击速度和%de_move_speed_rate%%移动速度。持续%duration%秒，冷却%cool%秒。
]],
    duration = 20,
    cool = 60,
    radius = 300,
    damage_reduce_rate = 50,
    damage_life_rate = 5,
    damage = function ( self )
        return self.owner:get_max_life() * self.damage_life_rate / 100
    end,
    de_attack_speed = 30,
    de_move_speed_rate = 30,
}

function mt:on_effect()
    local unit = self.owner
    
    if unit:find_buff('圣龙降临-变身') then
        unit:remove_buff('圣龙降临-变身')
    end

    unit:add_buff('圣龙降临-变身'){
        skill = self,
        time = self.duration,
        original_id = unit.id,
        radius = self.radius,
        damage_reduce_rate = self.damage_reduce_rate,
        damage_life_rate = self.damage_life_rate,
        de_attack_speed = self.de_attack_speed,
        de_move_speed_rate = self.de_move_speed_rate,
    }
    
end

local buff = ac.buff['圣龙降临-变身']{
    tranform_effect_model = [[Abilities\Spells\Orc\FeralSpirit\feralspirittarget.mdl]],
    tranform_id = 'O202',
    pulse = 1,
}

function buff:on_add(  )
    local unit = self.target
    unit:transform(self.tranform_id)
    unit:add_effect(self.tranform_effect_model, 'chest'):remove()
    self.damage_reduce_trg = unit:event '单位-即将受到伤害-乘除'(function ( trg, damage )
        damage.damage = damage.damage * (1 - self.damage_reduce_rate/100)
    end)
end

function buff:on_pulse(  )
    local unit = self.target
    local damage = unit:get_max_life() * self.damage_life_rate / 100

    for _, u in ac.selector()
        :in_range(unit, self.radius)
        :is_enemy(unit)
        :ipairs()
    do
        unit:damage{
            target = u,
            damage = damage,
            damage_type = '魔法',
        }
        u:add_effect(self.damage_effect_model, 'chest'):remove()
        u:add_buff('圣龙降临-缓速'){
            source = unit,
            skill = self.skill,
            de_attack_speed = self.de_attack_speed,
            de_move_speed_rate = self.de_move_speed_rate,
        }
    end

end

function buff:on_remove(  )
    local unit = self.target
    unit:transform(self.original_id)
    if self.damage_reduce_trg then
        self.damage_reduce_trg:remove()
    end
end

local buff_2 = ac.buff['圣龙降临-缓速']{
    time = 1,
    model = [[Abilities\Spells\Items\OrbVenom\OrbVenomSpecialArt.mdl]],
}

function buff_2:on_add(  )
    local unit = self.target
    unit:add_attack_speed(-self.de_attack_speed)
    self.de_move_speed = unit:add_move_speed(0, -self.de_move_speed_rate)
    self.eff = unit:add_effect(self.model, 'chest')
end

function buff_2:on_remove(  )
    local unit = self.target
    unit:add_attack_speed(self.de_attack_speed)
    unit:add_move_speed(-self.de_move_speed)
    if self.eff then
        self.eff:remove()
    end
end
