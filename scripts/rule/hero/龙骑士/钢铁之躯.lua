local mt = ac.skill['钢铁之躯']{
    ability_id = 'A302',
    recovery = {5, 15, 30, 50, 80},
    layer = 10,
    time = 20,
    passive = true,
    positive = false,
    tip = [[
每次受到攻击增加%recovery%点生命恢复，最高叠加%layer%次，持续%time%秒。
]],
}

function mt:on_enable()
    local unit = self.owner
    unit:add_buff('龙骑士-钢铁之躯'){
        skill = self,
    }
end

function mt:on_disable()
    local unit = self.owner
    unit:remove_buff('龙骑士-钢铁之躯')
end

local mt = ac.buff['龙骑士-钢铁之躯']{

}

function mt:on_add()
    local unit = self.target
    local skill = self.skill
    self.trg = unit:event '单位-即将受到伤害效果'(function(trg, damage)
        unit:add_buff('龙骑士-钢铁之躯-恢复'){
            skill = skill,
            cover_max = skill.layer,
            recovery = skill.recovery,
            time = skill.time,
        }
    end)
end

function mt:on_remove()
    if self.trg then
        self.trg:remove()
    end
end


local mt = ac.buff['龙骑士-钢铁之躯-恢复']{
    cover_type = 1,
}

function mt:on_add()
    local unit = self.target
    local skill = self.skill
    unit:add_life_recovery(self.recovery)
end

function mt:on_remove()
    local unit = self.target
    local skill = self.skill
    unit:add_life_recovery(-self.recovery)
end