local mt = ac.skill['喷火']{
    ability_id = 'A304',
    effect_model = [[Abilities\Spells\Other\BreathOfFire\BreathOfFireMissile.mdl]],
    radius = 250,
    tip = [[
对扇形区域的敌人造成%moment_damage%点伤害,此后对命中敌人每秒灼烧%last_damage%点伤害,持续%duration%秒。冷却%cool%秒。
]],
    moment_damage = function ( self )
        return self.owner:get_max_life() * 20 / 100
    end,
    last_damage = function ( self )
        return self.owner:get_max_life() * 1 / 100
    end,
    duration = 10,
    cool = 15,
}

function mt:on_effect()
    local unit = self.owner
    local ang = unit:get_facing()
    local lv = self:get_level()
    local damage = self.moment_damage
    local high = unit:get_high()
    local mvr = ac.mover.line{
        start = (unit:get_point() - {ang, 40}),
        angle = ang,
        distance = 600,
        model = self.effect_model,
        speed = 1200,
        height = 50 + high / 3,
        skill = self,
    }
    if mvr then
        local skill = self
        local g = {}
        function mvr:on_move()
            local p = self.mover:get_point() - {ang, -40}
            for _, u in ac.selector()
            :in_sector(p, skill.radius, ang, 120)
            :add_filter(function(u)
                return not g[u]
            end)
            :is_enemy(unit)
            :ipairs()
            do
                g[u] = true
                unit:damage{
                    target = u,
                    damage = damage,
                    skill = skill,
                    damage_type = '魔法',
                }
                u:add_buff('喷火-灼烧'){
                    source = unit,
                    skill = skill,
                    time = skill.duration,
                }
            end
        end

    end
end

local buff = ac.buff['喷火-灼烧']{
    model = [[Abilities\Spells\Other\BreathOfFire\BreathOfFireDamage.mdl]],
    pulse = 1,
}

function buff:on_add(  )
    local unit = self.target
    self.effect = unit:add_effect(self.model, 'chest')
end

function buff:on_pulse(  )
    local unit = self.target
    local source = self.source
    local damage = self.skill.last_damage
    source:damage{
        target = unit,
        damage = damage,
        skill = self.skill,
        damage_type = '魔法',
    }
end

function buff:on_remove(  )
    if self.effect then
        self.effect:remove()
    end
end